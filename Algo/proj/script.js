$(document).ready(function(){
   //console.log("siema"); 
    
    $('#startGame').click(function() { //coś ma się stać po naciśnięciu przycisku.
       var rangeFrom =  parseInt($('#rangeFrom').val()),
           rangeTo =  parseInt($('#rangeTo').val()),
           lives =  parseInt($('#lives').val()),
           randomizedNumber,
           guessTries = 1,//liczba żyć...
           $heading = $('h1'),
           $subHeading = $('p');
               
           
        if(rangeFrom > 0 && rangeTo > rangeFrom && lives >0){
            //console.log(rangeFrom, rangeTo, lives);
            $(".settingsBox").css('display', 'none');
            
            $(".playBox").css('display', 'block');
            
            randomizedNumber = (Math.random() * (rangeTo - rangeFrom) + rangeFrom)|0;
            
            console.log('Wylosowany numer to: ' + randomizedNumber);
            
            $('#guessButton').click(function(){
                var guessNumber = parseInt($('#guessNumber').val());
               
               if((guessTries-1) <= lives){
                    if(guessNumber == randomizedNumber){
                        $heading.html("Wygrałeś!!!<br/> Twoja liczba jest taka sama jak liczba przeciwnika! :D").css('color', 'green');
                        $(".playBox").css('display', 'none');
                        $subHeading.css('display', 'none');
                    }else if( guessNumber > randomizedNumber){
                        $heading.html("Twoja liczba jest większa od liczby przeciwnika");
                    }else{
                        $heading.html("Przegrałeś leszczu");
                    }
                    guessTries++;
                }else{
                    $heading.html('Przegrana! Koniec gry.').css('color','red');
                    $subHeading.html('');
                    $(".playBox").css('display', 'none');
                }
                $subHeading.html('Próba ' + guessTries + '/' + lives);
            });
            $subHeading.html('Próba ' + guessTries + '/' + lives);
        }else{
            $heading.html("Podano złe ustawienia!").css('color','red');
        }
        
    });
});