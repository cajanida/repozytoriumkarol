$(document).ready(function() {
	var points = 0;
	
	function makeDeck() {
		var colors = ['♣', '♠', '♥', '♦'], 
			figures = ['A', 'K', 'Q', 'J'],
			deck = [];
			
		for(var i = 2; i <= 10; i++) {
			figures.push(i.toString());
		}
		// wygenerujmy sobie talie
		// kazdy element z kolorow musi posiadac kazdy odpowiednik w figurach
		// ['A trefl', 'A pik', 'A kier', 'A karo', 'K trefl', 'K pik' ...]
		for(var i in figures) {
			for(var j in colors) {
				deck.push(figures[i] + " " + colors[j]);
				//console.log(figures[i], colors[j]);
			}
		}
		return deck;
	}
	
	// funkcja randomizujaca tablice
	function shuffleArray(arr) {
		var rand, temp;
		for(var i = arr.length; i > 0; i--) {
			rand = (Math.random() * i) | 0;
			temp = arr[i - 1];
			arr[i - 1] = arr[rand];
			arr[rand] = temp;
		}
		return arr;
	}
	
	function countPoints(arr) {
		/*
		napiszmy kod, ktory pozwoli na dodanie 1, a nie 11 punktow, 
		w przypadku Asa i gdy punkty przekraczaja 21
		*/
		
		var points = 0, pt;
		for(var i in arr) {
			pt = parseInt(arr[i]);
			if(isNaN(pt)) {
				points += 10;
			} else if(pt > 0) {
				points += pt;
			}
			if(arr[i][0] == 'A') {
				if(points >= 21) points -= 9;
				else points++;
			}
		}
		return points;
	}
	
	
	/* wygrana */
	function win(points) {
		/*
		1. wyswietlenie komunikatu
		2. updatePoints();
		*/
		$('#myModal').modal({'show' : 'true'});
		$('.modal-body').html('Brawo! Wygrana, oby tak dalej.');
		points++;
		updatePoints(points);
	}
	
	/* przegrana */
	function lost(points) {
		/*
		1. wyswietlenie komunikatu
		*/
		$('#myModal').modal({'show' : 'true'});
		$('.modal-body').html('Niestety, tym razem przegrana.');
		points--;
		updatePoints(points);
	}
	
	function tie() {
		$('#myModal').modal({'show' : 'true'});
		$('.modal-body').html('Nienajgorzej, remis. Mogło być lepiej, ale mogło być i gorzej.');
	}
	
	
	/* naprawa nadpisywania punktow */
	function updatePoints(points) {
		var suffix = 'punkty';
		/*
		napiszmy kod, ktory w zaleznosci od ilosci punktow, 
		wstawi odpowiedni suffix tj.
		1 punkt
		2, 3, 4 punkty
		5+ punktów
		*/
		points = parseInt(points);
		if(points == 1) suffix = 'punkt';
		else if(points >= 5) suffix = 'punktów';
		$('.points').html(points);
		$('.ptInfo').html(suffix);
	}
	
	// przypisanie karty dla gracza i komputera
	
	function initGame() {
		var deck = makeDeck(), playerCard1, playerCard2, playerPoints, computerPoints, computerCard1, computerCard2, points = parseInt($('.points').html());
		if(isNaN(points)) points = 0;
		
		shuffleArray(deck);
		playerCard1 = deck.pop();
		playerCard2 = deck.pop();
		computerCard1 = deck.shift();
		computerCard2 = deck.shift();
		
		console.log(computerCard1);
		$('.card:nth-child(1)').html(playerCard1);
		$('.card:nth-child(2)').html(playerCard2);
		$('.row:nth-child(4) > .card:nth-child(1)').html(computerCard1);
		$('.row:nth-child(4) > .card:nth-child(2)').html(computerCard2);
		
		playerPoints = countPoints([playerCard1, playerCard2]);
		computerPoints = countPoints([computerCard1, computerCard2]);
		
		$('.checkBtn').click(function() {
			$('#myModal').modal({'show' : 'false'});
			
			if(playerPoints > computerPoints) {
				win(points);
			} else if(playerPoints == computerPoints) {
				tie();
			} else {
				lost(points);
			}
		});
	}
	$('.newGameBtn').click(function() {
		initGame();
	});
	initGame(points);
	
	/*
	Mechanizm dobierania kart.
	Karte można dobrać w dowolnym momencie, przed sprawdzeniem.
	Kartę można dobrać tylko, gdy posiadamy mniej niż 21 punktów.
	Przekroczenie liczby 21 po dobraniu karty skutkuje przegraną.
	*/
});