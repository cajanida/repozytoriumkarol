$(document).ready(function(){
   var question = [
       { "question":"Ile czasu potrzeba, aby przejść z punktu A do punktu B?",
         "answer": ["5 godzin" , "2 lata", "3 minuty", "Weź się tato!"],
         "correctAnswer": "2 lata",
         "tip" : "Więcej niż 3 minuty."
       },
       {
         "question":"Dlaczego szkolenie nazywa się Programista Python?",
         "answer": ["Bo tak" , "Bo papiery podpisane", "Przecież front", "Weź się PUP!"],
         "correctAnswer": "Bo papiery podpisane",
         "tip" : "Przecież jest za free." 
       },
       {
         "question":"Dlaczego szkolenie odbywa się w Sztuka Wyboru?",
         "answer": ["Bo tak" , "Bo papiery podpisane", "Brak prądu", "Weź się Marian!"],
         "correctAnswer": "Brak prądu",
         "tip" : "Hipsterzy grasują." 
       }
   ], usedIndexes = [], currentQuestion = getRandomQuestion(question, usedIndexes); 
    
    function fillData(obj){
        getInputsUndisabled();
        $(".ques").html(obj["question"]);
        $('.answers').each(function(i, e){
            e.innerHTML = obj["answer"][i];
        });
    }
    //metoda losuje i ustawia aktualne pytanie
    function getRandomQuestion(question, usedIndexes){
        return question[getRandomIndex(question, usedIndexes)];
    }
    
    function getRandomIndex(question, usedIndexes){
        var elem = question.length,
            tempArr = [], random;
        for(var i=0; i<elem; i++){
            if(usedIndexes.indexOf(i) == -1) tempArr.push(i);
        }
        random = (Math.random() * tempArr.length) | 0;
        usedIndexes.push(tempArr[random]);
        return tempArr[random];
    }
    //metoda losująca i ustawiająca kolejne pytanie
    
    function getRandomQuestion(question){
        var index = getRandomIndex(question, usedIndexes);
        if(index == undefined){
            winGame();
        }
        return question[index];
    }
    
    function endGame(){
        $(".answers, .ques").hide();
    }
    function winGame(){
        
        $(".answers, .ques").hide();
        alert("Wygrałeś dużą Bańkę!!!");
    }
    function getHelpFriend(currentQuestion){
        alert(currentQuestion["tip"]);
        $(".friendHelp").attr("disabled", 'disabled');
        return currentQuestion["tip"];
    }
    
    $('.friendHelp').click(function(){
        getHelpFriend(currentQuestion);
    });
    $('.fiftyFiftyHelp').click(function(){
        getHelpFromComputer(currentQuestion);
    });
    $('.audienceHelp').click(function(){
        //getHelpFriend(currentQuestion);
    });
    
    function getHelpFromComputer(){
        var tempArr = [];
        $(".answers").each(function(i,e){
            if(e.innerHTML != currentQuestion["correctAnswer"]){
                tempArr.push(e);
            }
        });
        tempArr.pop().disabled = 'disabled';
        tempArr.pop().disabled = 'disabled';
        $('.fiftyFiftyHelp').attr('disabled', 'disabled');
    }
    function getInputsUndisabled(){
        $('.answers').each(function(i,e){
            $(this).removeAttr('disabled');
        });
    }
    
    $('.answers').click(function(){
       if($(this).html() == currentQuestion["correctAnswer"]){
           console.log("Brawo");
           //wywołujemy metodę która załaduje nam kolejne pytanie
           currentQuestion = getRandomQuestion(question, usedIndexes);
           fillData(currentQuestion);
       }else{
           console.log("zle");
           endGame();
       }
    });
    fillData(currentQuestion);
    
    
    
    
});