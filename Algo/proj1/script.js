$(document).ready(function(){
   var wordsDictionary = ['oko', 'konik', 'hangar', 'pyton', 'skuter', 'ostro','piwko'],
       currentWord = wordsDictionary[(Math.random() * wordsDictionary.length)|0].toLowerCase(),
       currentUserWord = shadowWord(currentWord);
    console.log(currentWord);
    
    $('.wordCount').html(currentUserWord);
    $('.letter').click(function(){
        var currentLetter = $(this).val().toLowerCase(), 
            currentLetterIndexes = [];
        for(var i = 0; i < currentWord.length; i++){
            if(currentWord[i] == currentLetter){
                console.log('Zgadnięto literkę ' + currentLetter + ' pod indeksem ' + i );
                currentLetterIndexes.push(i);
            }
        }
        if(currentLetterIndexes.length == 0){
            $(this).attr('disabled', 'disabled').css({'background-color':'green', 'border':'0'});
        }
        currentUserWord = unhideLetters(currentUserWord, currentLetter, currentLetterIndexes);
        $('.wordCount').html(currentUserWord);
    });
 
});

   function shadowWord(str){
        return "_".repeat(str.length);
       
    }
function unhideLetters(sentence, letter, indexes){
    var tempArr = sentence.split("");
    if(indexes.length > 0){
        for(var i in indexes){
        tempArr[indexes[i]] = letter;
        }
    }
    
    return tempArr.join("");
}